class Job {
    constructor(title, description, minimumSalary, maximumSalary, location, postingDate, recruiter) {
        this.title = title;
        this.description = description;
        this.minimumSalary = minimumSalary;
        this.maximumSalary = maximumSalary;
        this.location = location;
        this.postingDate = new Date(postingDate);
        this.recruiter = recruiter;
        return this;
    }
}