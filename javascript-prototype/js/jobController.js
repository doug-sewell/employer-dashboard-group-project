const jobController = (function() {
    const filterJobSearch = (jobs, userSearch) => {
        let filteredJobs = [];
        let checkedUserSearch = userSearch.toLowerCase().trim();
    
        filteredJobs = jobs.filter(job => {
            let checkedJobInfo = new Job(job.title.trim().toLowerCase(), job.description.trim().toLowerCase());
    
            if (checkedJobInfo.title.includes(checkedUserSearch) || checkedJobInfo.description.includes(checkedUserSearch)) {
                return true;
            }
        });
        return filteredJobs;
    }
    
    return {
        createFilteredJobsArray: (jobs, userSearch) => {
            return filterJobSearch(jobs, userSearch);
        }
    }

})();