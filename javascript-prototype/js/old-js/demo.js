/*
This file should be deleted for production.
*/
console.info('demo.JS utilized. Delete for production.');

const demoJobs = (jobs) => {
    jobs.push(new Job('Front End Web Dev', 'Know HTML, CSS, and JavaScript', 10, 9999999999, 'Charlotte, NC', 'November 13, 2019', 'Bob Vance'));
    jobs.push(new Job('QA', 'Debug and fix code',900,235235234,'Richmond, VA','December 8, 2018','Michael Scott'));
    jobs.push(new Job('Marketer', 'Market the new web project.',80000,100000,'Los Anges, CA','September 9, 2019', 'Stanley Hudson'));
    jobs.push(new Job('Software Developer', 'Be an expert in Java',30000,50000,'Orlando, FL', 'October 28, 2019', 'Andy Bernard'));
    jobs.push(new Job('Android Developer', 'Be an expert in Java, Kotlin, React Native, or Native Script', 40000,60000, 'Scranton, PA', 'February 7, 2019', 'Dwight Schrute'));
    jobs.push(new Job('iOS Developer', 'Be an expert in Swyft',20000,30000,'Seattle, WA', 'March 20, 2019', 'Kelly Kapoor'));
    jobs.push(new Job('Back End Developer', `Be an expert in Node.JS, Java, C#, PHP, or other backend language and/or framework.`, 10, 9999999999, 'Charlotte, NC', 'November 13, 2019', 'Bob Vance'));
    jobs.push(new Job('Full Stack Developer', 'See requirements of Front End and Back End Developer roles', 900, 235235234,'Richmond, VA','December 8, 2018','Michael Scott'));
    jobs.push(new Job('Software Developer Utilizing Web Technologies', 'Utilize front end technologies to create MacOS and Windows 10 software using Electron.',80000,100000,'Los Anges, CA','September 9, 2019', 'Stanley Hudson'));
    jobs.push(new Job('React Native Developer', 'Create Native mobile applications for both Android and iOS utilizing React Native',30000,50000,'Orlando, FL', 'October 28, 2019', 'Andy Bernard'));
    jobs.push(new Job('Material Design Web Designer', `Understand Google's best practices for Material Design to create responsive layouts`, 'Be an expert in Java, Kotlin, React Native, or Native Script', 40000,60000, 'Scranton, PA', 'February 7, 2019', 'Dwight Schrute'));
    jobs.push(new Job('Kotlin Developer', 'Be familiar with JVM and core Kotlin concepts.',20000,30000,'Seattle, WA', 'March 20, 2019', 'Kelly Kapoor'));
/*    jobs.push(new Job('JavaScript Engineer', 'Expert in front-end development, including JSON, objectect oriented programming (OOP)'));
    jobs.push(new Job('PHP Developer', 'Create server-side code utilizing PHP.'));
    jobs.push(new Job('Figma Web Designer', 'Utilize Figma for rapid prototyping'));
    jobs.push(new Job('Adobe XD Designer', 'Utilize Adobe XD for rapid prototyping'));
    jobs.push(new Job('CSS Designer', 'Utilize CSS for creative web design'));
    jobs.push(new Job('Web Accessibility Engineer', 'Know all aspects of WCAG 2.1 guidelines for web accessibility engineering'));
    jobs.push(new Job('UX Researcher', 'Conduct studies on UX for improvement of enterprise services'));
    jobs.push(new Job('Angular Developer', 'Utilize cutting edge front end technologies and leverage the Angular framework for UI development.'));
    jobs.push(new Job('React Developer', 'Utilize cutting edge front end technologie and leverage the React library for UI development'));
    jobs.push(new Job('Vue Developer', `Utilize cutting edge front end technologies and leverage the Vue framework for UI development`));
    jobs.push(new Job('Progressive Web App (PWA) Developer', `Update legacy web projects into PWA's as well as develop new PWA's for clients`));
    jobs.push(new Job('Firebase Full Stack Developer', `Utilize HTML, CSS, and JavaScript with Firebase for full stack development.`));*/
    return jobs;
} 