/* 
README:
Further work needs to be done for code organization for more organized data flow as well as implement better modularity. 
However, this is a working prototype. 
Code is not final and has bugs still to be ironed out.

IMPROVEMENT TODO'S:
- Bug Fix: Remove event handler for the enter key after it's pressed.
- The page uses a standard browser alert if the user does a blank search. Replace browser alert with a modal for better UX.
*/

//INIT
const init = () => {
    uiManipulation();
    displayHome();
    resetSearchEventListener();

    // document.getElementById('display-jobs').addEventListener('click', () => {
    //     displayJobs(jobs);
    //     lookForUserJobClick();
    // });

    // document.getElementById('logo').addEventListener('click', () => {
    //     displayHome();
    //     resetSearchEventListener();
    // });
}


//DATA
init(); //initializes app
let jobs = []; //Holds job objects for manipulation. 



