/*const uiManipulation = () => {
//     displayHome = () => {
//         document.getElementById('main-content').innerHTML = `
//     <section class="d-flex justify-content-center align-items-center flex-column w-auto min-vh-100" id="home">
//     <h1 class="sr-only">Job Search</h1>
//     <img src="assets/mission-code-logo.png" alt="Mission Code Logo">
//     <form class="w-100">
//         <div class="form-row align-items-center m-0">
//             <div class="w-100 d-flex justify-content-center">
//                 <label class="sr-only" for="search-bar">Job Search</label>
//                 <div class="input-group mb-2 d-flex justify-content-center w-75">
//                     <input type="text" class="form-control rounded-0 border-right-0 pt-4 pb-4"
//                         id="search-bar"
//                         aria-label="Search for job postings by keyword, job title, or company"
//                         placeholder="Search for job postings by keyword, job title, or company">
//                     <div class="input-group-append">
//                         <span class="input-group-text border-left-0 bg-white rounded-0"
//                             id="search-bar_magnifying-glass"><i class="fas fa-search"></i></span>
//                     </div>
//                 </div>
//             </div>
//         </div>
//     </form>
// </section>
//     `;
//     }

    // displayJobs = (jobs) => {
    //     let html;

    //     if (jobs.length > 0) {
    //         html = `<div class="container-fluid" id="job-postings">
    //             <div class="d-flex align-items-center flex-column custom-header-footer-content-clear-margin">
    //             `;
    //         for (let x = 0; x < jobs.length; x++) {
    //             html += `
    //          <div class="w-75 text-center border border-dark mt-3 custom-job-option" data-index-number=${x} data-job-title="${jobs[x].title}" data-job-description="${jobs[x].description}">
    //              <p class="m-0">${jobs[x].title}<br>
    //                  ${jobs[x].description}<br>
    //                  Salary Range: $${jobs[x].minimumSalary} - $${jobs[x].maximumSalary}<br>
    //                  Location: ${jobs[x].location}<br>
    //                  Date Posted: ${jobs[x].postingDate}<br>
    //                  Recruiter Hiring: ${jobs[x].recruiter}
    //              </p>
    //          </div>
    //      `
    //         }
    //         //This was likely meant to only display three jobs or so and this icon would show the rest. 
    //         //Unsure, so for now just placing at the end of job listings:
    //         html += `<p class="mt-3 h1" id="three-dot-menu"><i class="fas fa-ellipsis-v"></i></p>`;

    //     } else {
    //         html = `<div class="container-fluid" id="job-postings">
    //             <div class="d-flex align-items-center justify-content-center flex-column custom-header-footer-content-clear-margin min-vh-100 text-center">
    //                 <p class="h3">There aren't any openings at this time.<br>Please check back soon and thank you for your interest!</p>
    //             `;
    //     }
    //     html += `</div>
    // </div>
    // `;
    //     document.getElementById(`main-content`).innerHTML = html;
    // }

//     displayJobApplication = (title, description) => {
//         let html = `<section class="d-flex justify-content-center align-items-center w-auto min-vh-100" id='job-applicant-ui'>
//     <h2 class="sr-only">Job Application Form</h2>
//     <form class=" w-75">
//         <div class="container-fluid text-center" id="job-information">
//             <p>${title}<br>
//                 ${description}
//             </p>
//         </div>
//         <div class="form-group row">
//             <label for="input-name" class="col-sm-3 col-form-label text-right">Name</label>
//             <div class="col-sm-9">
//                 <input type="text" class="form-control" id="input-name">
//             </div>
//         </div>
//         <div class="form-group row">
//             <label for="input-address" class="col-sm-3 col-form-label text-right">Address</label>
//             <div class="col-sm-9">
//                 <input type="text" class="form-control" id="input-address">
//             </div>
//         </div>
//         <div class="form-group row">
//             <label for="input-email" class="col-sm-3 col-form-label text-right">Email</label>
//             <div class="col-sm-9">
//                 <input type="email" class="form-control" id="input-email">
//             </div>
//         </div>
//         <div class="form-group row">
//             <label for="input-phone" class="col-sm-3 col-form-label text-right">Phone Number</label>
//             <div class="col-sm-9">
//                 <input type="text" class="form-control" id="input-phone">
//             </div>
//         </div>
//         <div class="form-group row">
//             <label for="resume-upload" class="col-sm-3 col-form-label text-right pt-3 pb-3">Resume*</label>
//             <div class="custom-file col-sm-9">
//                 <input type="file" class="d-none" id="resume-upload">
//                 <label for="resume-upload" class="btn btn-primary pt-3 pb-3">Upload File</label>
//             </div>
//         </div>
//     </form>
// </section>`
//         document.getElementById(`main-content`).innerHTML = html;
//     }

 //   resetSearchEventListener = () => {

        // const performSearch = () => {
        //     let query = userSearchQuery();
        //     if (!query) {
        //         alert('A valid search query is required. Please type in a desired job.');
        //     } else {
        //         let newJobs = createFilteredJobsArray(jobs, query);
        //         displayJobs(newJobs);
        //         lookForUserJobClick();
        //     }
        // }

        // document.getElementById('search-bar_magnifying-glass').addEventListener('click', () => {
        //     performSearch();
        // });

        // document.addEventListener('keydown', (e) => {
        //     if (e.keyCode === 13) {
        //         performSearch();
        //     }
        // });
    }

    // userSearchQuery = () => {
    //     return document.getElementById('search-bar').value.trim(); //The trim eliminates searches with only spaces.
    // }

//}
*/