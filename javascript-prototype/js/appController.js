/*
For Production: Remove demo as an argument and parameter in the controller.
Remove all associated functions with the demoJobs as well. 
*/

/*
TODO:
1. Fix search bug in which doing multiple searcher throws an error in the console.
2. Displaying a search validation error in the UI currently does a default browser alert. Change
to a modal.
*/

var controller = ((ui, dataHandler, demo) => {

    const jobs = [];
    let searchQuery = '';

    const displayHome = () => {
        ui.displayHome();
    }

    const resetSearchListeners = () => {
        document.getElementById('search-bar_magnifying-glass').addEventListener('click', () => {
            performSearch();
        });

        document.addEventListener('keydown', (e) => {
            if (e.keyCode === 13) {
                performSearch();
            }
        });
    }

    const resetGlobalListeners = () => {
        document.getElementById('display-jobs').addEventListener('click', () => {
            ui.displayJobs(jobs);
            lookForUserJobClick();
        });

        document.getElementById('logo').addEventListener('click', () => {
            displayHome();
            resetSearchListeners();
        });
    }

    const performSearch = () => {
        searchQuery = ui.getSearchQuery();
        if (!searchQuery) {
            ui.displayValidationError();
        } else {
            let newJobs = dataHandler.createFilteredJobsArray(jobs, searchQuery);
            ui.displayJobs(newJobs);
            lookForUserJobClick();
        }
    }

    const lookForUserJobClick = () => {
        const jobs = document.querySelectorAll('.custom-job-option');
        jobs.forEach(job => {
            job.addEventListener('click', e => {
                ui.displayJobApplication(e.currentTarget.dataset.jobTitle, e.currentTarget.dataset.jobDescription);
            });
        });
    }

    const demoJobCreation = (jobs) => {
        demo.createDemoJobs(jobs);
    }

    const consoleLogJobs = () => {
        console.log(jobs);
    }

    return {
        initializeApplication: () => {
            console.log('App initialized');
            displayHome();
            resetSearchListeners();
            resetGlobalListeners();
            demoJobCreation(jobs); //Delete in production
        },
        displayHomeScreen: () => {
            displayHome();
        },
        consoleLogJobs: () => { //Delete in production
            console.log(jobs);
        }
    }

})(uiManipulation, jobController, createDemoJobs);



controller.initializeApplication();

//NOTE: You need to call the function in demo.js for the demo jobs to be created.


