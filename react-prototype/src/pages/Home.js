import React from 'react';
import Header from '../shared-components/Header';
import JobSearch from '../shared-components/JobSearch';
import Footer from '../shared-components/Footer';

const Home = () => {
    return (
        <JobSearch />
    )
}

export default Home;