import React from 'react';
import { Link } from 'react-router-dom';


const UpdateJob = (props) => {
    const [jobFormVal, setJobFormVal] = React.useState(props.location.job);

    return (
        <section className="custom-header-footer-content-clear-margin container" id="job-applicant-ui">
            <header className="mb-4 d-flex">
                <h1 className="d-inline-block h4">JavaScript Engineer</h1>
                <Link to={{
                    pathname: "/applicant-list",
                    job: props.location.job
                }} className="btn custom-misssion-code-bg font-weight-bold pl-4 pr-4 ml-auto" href="view-applicants.html">VIEW
                APPLICANTS</Link>
            </header>
            <div className="">
                <form onSubmit={handleFormSubmit}>

                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label" htmlFor="job-title">Job Title</label>
                        <div className="col-sm-4">
                            <input onChange={handleTitleChange} className="form-control" type="text" id="job-title" value={jobFormVal.title} />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label" htmlFor="location">Location</label>
                        <div className="col-sm-4">
                            <input onChange={handleLocationChange} className="form-control" type="text" id="location" value={jobFormVal.location} />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label" htmlFor="date-posted">Date Posted</label>
                        <div className="col-sm-4">
                            <input onChange={handleDateChange} className="form-control" type="text" id="date-posted" value={jobFormVal.date} />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label" htmlFor="minimum-salary">Minimum Salary</label>
                        <div className="col-sm-4">
                            <input onChange={handleMinSalaryChange} className="form-control" type="text" id="minimum-salary" value={jobFormVal.minSalary} />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label" htmlFor="maximum-salary">Maximum Salary</label>
                        <div className="col-sm-4">
                            <input onChange={handleMaxSalaryChange} className="form-control" type="text" id="maximum-salary" value={jobFormVal.maxSalary} />
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label" htmlFor="recruiter">Recruiter Name</label>
                        <div className="col-sm-4">
                            <input onChange={handleRecruiterChange} className="form-control" type="text" id="recruiter" value={jobFormVal.recruiter} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label" htmlFor="recruiter">Job Description</label>
                        <div className="col-sm-4">
                            <textarea
                                defaultValue={jobFormVal.description}
                                className="form-control"
                                rows="4"
                                id="job-description"
                                onChange={handleDescriptionChange} ></textarea>
                        </div>
                    </div>

                    <div className="modal fade" id="update-changes-ask" tabIndex="-1" role="dialog"
                        aria-labelledby="update-changes-ask-modal" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">

                                <div className="modal-body">
                                    <h5 id="update-changes-ask-modal">Do you want to save the changes you made to this job
                            posting?</h5>
                                </div>
                                <div className="modal-footer">


                                    <button type="button" className="btn btn-secondary" data-dismiss="modal" >Cancel</button>




                                    <Link to={{
                                        pathname: '/jobs',
                                        jobs: props.location.jobs,
                                        updatedJob: {
                                            id: jobFormVal.id,
                                            title: jobFormVal.title,
                                            location: jobFormVal.location,
                                            date: jobFormVal.date,
                                            recruiter: jobFormVal.recruiter,
                                            minSalary: jobFormVal.minSalary,
                                            maxSalary: jobFormVal.maxSalary,
                                            description: jobFormVal.description
                                        },
                                        updateDB: true
                                    }}><button type="submit" className="btn btn-primary" >Yes, Continue</button></Link>





                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="modal fade" id="delete-job-ask" tabIndex="-1" role="dialog"
                        aria-labelledby="delete-job-ask-modal" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h5 id="delete-job-ask-modal">Are you sure you want to delete this job posting? You cannot
                            undo this action.</h5>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>



                                    <Link to={{
                                        pathname: '/jobs',
                                        jobs: props.location.jobs,
                                        updatedJob: {
                                            id: jobFormVal.id,
                                            title: jobFormVal.title,
                                            location: jobFormVal.location,
                                            date: jobFormVal.date,
                                            recruiter: jobFormVal.recruiter,
                                            minSalary: jobFormVal.minSalary,
                                            maxSalary: jobFormVal.maxSalary,
                                            description: jobFormVal.description
                                        },
                                        deleteJob: true
                                    }}>
                                        <button type="button" className="btn btn-primary">Yes, Continue</button>
                                    </Link>





                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div className="d-flex align-content-end">
                    <div className="mt-auto pb-5 d-flex align-content-center">
                        <button className="border-0 bg-transparent" data-toggle="modal" data-target="#delete-job-ask"><i
                            className="fas fa-trash-alt fa-2x text-danger m-0 p-0"></i></button>



                        <Link className="btn btn-outline-secondary ml-2 mr-2" to={{
                            pathname: '/jobs',
                            jobs: props.location.jobs,
                            updatedJob: null,
                            updateDB: false
                        }}>Cancel</Link>
                        <button type="button" data-toggle="modal" data-target="#update-changes-ask"
                            className="btn custom-misssion-code-bg font-weight-bold pl-4 pr-4">UPDATE</button>
                    </div>
                </div>
            </div>

        </section>
    )
    function handleTitleChange($event) {
        setJobFormVal({
            id: jobFormVal.id,
            title: $event.target.value,
            location: jobFormVal.location,
            date: jobFormVal.date,
            recruiter: jobFormVal.recruiter,
            minSalary: jobFormVal.minSalary,
            maxSalary: jobFormVal.maxSalary,
            description: jobFormVal.description
        });
    }
    function handleLocationChange($event) {
        setJobFormVal({
            id: jobFormVal.id,
            title: jobFormVal.title,
            location: $event.target.value,
            date: jobFormVal.date,
            recruiter: jobFormVal.recruiter,
            minSalary: jobFormVal.minSalary,
            maxSalary: jobFormVal.maxSalary,
            description: jobFormVal.description
        });
    }
    function handleDateChange($event) {
        setJobFormVal({
            id: jobFormVal.id,
            title: jobFormVal.title,
            location: jobFormVal.location,
            date: $event.target.value,
            recruiter: jobFormVal.recruiter,
            minSalary: jobFormVal.minSalary,
            maxSalary: jobFormVal.maxSalary,
            description: jobFormVal.description
        });
    }
    function handleRecruiterChange($event) {
        setJobFormVal({
            id: jobFormVal.id,
            title: jobFormVal.title,
            location: jobFormVal.location,
            date: jobFormVal.date,
            recruiter: $event.target.value,
            minSalary: jobFormVal.minSalary,
            maxSalary: jobFormVal.maxSalary,
            description: jobFormVal.description
        });
    }
    function handleMinSalaryChange($event) {
        setJobFormVal({
            id: jobFormVal.id,
            title: jobFormVal.title,
            location: jobFormVal.location,
            date: jobFormVal.date,
            recruiter: jobFormVal.recruiter,
            minSalary: $event.target.value,
            maxSalary: jobFormVal.maxSalary,
            description: jobFormVal.description
        });
    }
    function handleMaxSalaryChange($event) {
        setJobFormVal({
            id: jobFormVal.id,
            title: jobFormVal.title,
            location: jobFormVal.location,
            date: jobFormVal.date,
            recruiter: jobFormVal.recruiter,
            minSalary: jobFormVal.minSalary,
            maxSalary: $event.target.value,
            description: jobFormVal.description
        });
    }
    function handleDescriptionChange($event) {
        setJobFormVal({
            id: jobFormVal.id,
            title: jobFormVal.title,
            location: jobFormVal.location,
            date: jobFormVal.date,
            recruiter: jobFormVal.recruiter,
            minSalary: jobFormVal.minSalary,
            maxSalary: jobFormVal.maxSalary,
            description: $event.target.value
        });
    }

    function handleFormSubmit($event) {
        $event.preventDefault();
        props.location.updateJob(jobFormVal);
    }

    function handleFormDelete($event) {
        $event.preventDefault();
        props.location.deleteJob(jobFormVal);
    }

}

export default UpdateJob;