import React from 'react';
import { Link } from 'react-router-dom';


const cards = (jobs) => {

    const cardsMapped = jobs.map(job => {


        function updateJob(job) {
            console.log('updating job', job);
            const database = window.firebase.database();
            //Function to write data to the jobs path:
            database.ref('jobs/' + job.id).set({
                title: job.title,
                description: job.description,
                location: job.location,
                minSalary: job.minSalary,
                maxSalary: job.maxSalary,
                recruiter: job.recruiter,
                date: job.date,
                id: job.id
            });
        }

        function deleteJob(job) {
            const database = window.firebase.database();

            database.ref('jobs/' + job.id).remove();
        }

        return (
            <div key={job.id} className="card shadow bg-light mb-5">
                <div className="card-body pl-3 pr-3 d-flex flex-column justify-content-sm-between">

                    <table className="table table-borderless table-sm">
                        <tbody>
                            <tr>
                                <th>
                                    Job Title:
                            </th>
                                <td>
                                    {job.title}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Location:
                                </th>
                                <td>
                                    {job.location}
                                </td>
                            </tr>
                            <tr>
                                <th>Date Posted:</th>
                                <td>{job.date}</td>
                            </tr>
                            <tr>
                                <th>Recruiter:</th>
                                <td>{job.recruiter}</td>
                            </tr>
                            <tr>
                                <th>Salary:</th>
                                <td>${job.minSalary} - ${job.maxSalary}</td>
                            </tr>
                            <tr>
                                <th>Description:</th>
                                <td>{job.description}</td>
                            </tr>
                        </tbody>
                    </table>
                    <footer className="h-auto w-100 d-flex justify-content-between mt-3 p-0">
                        <Link to={{
                            pathname: "/update-job",
                            updateJob: updateJob,
                            deleteJob: deleteJob,
                            jobs: jobs,
                            job: job
                        }} className="btn btn-primary">Edit Details</Link>
                        <Link to={{
                            pathname: "/applicant-list",
                            job: job
                        }} className="btn btn-outline-secondary">View Applicants</Link>
                    </footer>
                </div>
            </div>

        )
    });
    return cardsMapped;
}

function Jobs(props) {
    /*START:Removes Bootstrap modal styling*/
    document.querySelector('body').className = '';
    const modalBackdrop = document.querySelector('.modal-backdrop');
    if (modalBackdrop) {
        modalBackdrop.style.display = 'none';
    }
    /*END: Removes Bootstrap modal styling*/


    if (props.location.updatedJob) {
        props.location.jobs = props.location.jobs.map((job) => {
            if (job.id === props.location.updatedJob.id) {
                job.title = props.location.updatedJob.title;
                job.location = props.location.updatedJob.location;
                job.date = props.location.updatedJob.date;
                job.recruiter = props.location.updatedJob.recruiter;
                job.minSalary = props.location.updatedJob.minSalary;
                job.maxSalary = props.location.updatedJob.maxSalary;
                job.description = props.location.updatedJob.description;
            }
            return job;
        });
    }




    //Fetches data from database and sets them to be the jobs which will get rendered.
    React.useEffect(() => {
        const database = window.firebase.database();
        database.ref('jobs/').on('value', function (snapshot) {
            const jobsFromDB = snapshot.val();
            setJobs(jobsFromDB);
        });

        /*
        If there is a job that needs to be pushed to the database, this
        check will ensure the database gets updated.
        */
        if (props.location.updateDB) {
            const database = window.firebase.database();
            //Function to write data to the jobs path:
            database.ref('jobs/' + props.location.updatedJob.id).set({
                title: props.location.updatedJob.title,
                description: props.location.updatedJob.description,
                location: props.location.updatedJob.location,
                minSalary: props.location.updatedJob.minSalary,
                maxSalary: props.location.updatedJob.maxSalary,
                recruiter: props.location.updatedJob.recruiter,
                date: props.location.updatedJob.date,
                id: props.location.updatedJob.id
            });
        }

        if (props.location.deleteJob) {
            console.log('deleting job', props.location.updatedJob.id);
            const database = window.firebase.database();

            database.ref('jobs/' + props.location.updatedJob.id).remove();
            database.ref('applicants/' + props.location.updatedJob.id).remove();
        }






    }, []);

    //Sets the state using the jobs retrieved from the database.
    const [jobs, setJobs] = React.useState([]);








    return (
        <div className="container">
            <header className="custom-header-content-clear-margin d-flex py-4">
                <h1 className="display-5">Open Positions</h1>
                <div className="d-inline-block mt-auto mb-auto">
                    <a className="btn custom-misssion-code-bg ml-2 font-weight-bold pl-4 pr-4" href="#">ADD NEW JOB</a>
                </div>
            </header>
            <section className="card-columns">
                {cards(jobs)}
            </section>
        </div>

    )
}



export default Jobs;