import React from 'react';
import { Link } from 'react-router-dom';


const ApplicantList = (props) => {

    function jobRemoval(applicant, applicantID) {
        const database = window.firebase.database();
        database.ref('applicants/' + props.location.job.id + '/applicants/' + applicantID).remove();
    }

    function resumeAccepted(applicant, applicantID) {
        console.log(applicantID);
        const database = window.firebase.database();
        database.ref('applicants/' + props.location.job.id + '/applicants/' + applicantID).set({
            accepted: true,
            email: applicant.email,
            firstName: applicant.firstName,
            lastName: applicant.lastName,
            primaryPhone: applicant.primaryPhone,
            secondaryPhone: applicant.secondaryPhone,
            linkedInProfile: applicant.linkedInProfile,
            resume: applicant.resume
        })
    }

    const job = props.location.job;
    //Fetches data from database and sets them to be the jobs which will get rendered.
    React.useEffect(() => {
        const database = window.firebase.database();
        database.ref('applicants/' + props.location.job.id + '/applicants/').on('value', function (snapshot) {
            const applicantsFromDB = snapshot.val();
            console.log('applicantsFromDB', applicantsFromDB);
            setJobApplicants(applicantsFromDB);
        });

    }, []);

    const [jobApplicants, setJobApplicants] = React.useState([]);


    function displayApplicants(applicants) {
        const applicantsMapped = applicants.map((applicant, index) => {
            const modalID = `applicant-${index}-modal`;
            const modalTarget = `#${modalID}`;

            const modalRejectId = `applicant-${index}-reject-modal`;
            const rejectModalTarget = `#${modalRejectId}`;

            const modalAcceptedID = `applicant-${index}-accept-modal`;
            const acceptedModalTarget = `#${modalAcceptedID}`;


            return (
                <div key={index} className="w-100 d-flex justify-content-between border-bottom flex-wrap mb-2 pt-2 pb-2">
                    <a className="font-weight-bolder h3" href="javascript:;" data-toggle="modal" data-target={modalTarget}>{applicant.firstName} {applicant.lastName}
                        <i className="fas ml-2 fa-file"></i>
                    </a>
                    {applicant.accepted ? <p className="h3 font-weight-bolder text-success">Resume Accepted</p> : <div>
                        <button data-toggle="modal" data-target={rejectModalTarget}
                            className="btn btn-outline-secondary rounded-pill"><i
                                className="far fa-times-circle fa-2x text-danger"></i></button>
                        <button data-toggle="modal" data-target={acceptedModalTarget}
                            className="btn btn-outline-secondary rounded-pill ml-3 "><i
                                className="fas fa-check fa-2x text-success"></i></button>
                    </div>}
                    <div className="modal fade" id={modalID} tabIndex="-1" role="dialog"
                        aria-labelledby="applicant-0-resume" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="applicant-0-resume">Resume</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <h4>NAME</h4>
                                    <p>{applicant.firstName} {applicant.lastName}</p>
                                    <h4>EMAIL</h4>
                                    <p>{applicant.email}</p>
                                    <h4>PRIMARY PHONE</h4>
                                    <p>{applicant.primaryPhone}</p>
                                    <h4>SECONDARY PHONE</h4>
                                    <p>{applicant.secondaryPhone}</p>
                                    <h4>LINKEDIN</h4>
                                    <p>{applicant.linkedInProfile}</p>
                                    <p>{applicant.phone}</p>
                                    <h4>RESUME</h4>
                                    {applicant.resume}
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id={modalAcceptedID} tabIndex="-1" role="dialog"
                        aria-labelledby="applicant-0-accept" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="applicant-0-accept">CONFIRMATION</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <p className="h4 text-center">You are about to accept the following applicant:<br /><span
                                        className="font-weight-bolder">{applicant.firstName} {applicant.lastName}</span></p>
                                    <p className="h4 text-center">Would you like to proceed?</p>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button onClick={() => { resumeAccepted(applicant, index) }} data-dismiss="modal" data-toggle="modal" data-target="#applicant-0-notified-modal"
                                        type="button" className="btn btn-primary">Yes, Continue</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id={modalRejectId} tabIndex="-1" role="dialog"
                        aria-labelledby="applicant-0-reject" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="applicant-0-reject">CONFIRMATION</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <p className="h4 text-center">You are about to <span
                                        className="text-danger font-weight-bolder">reject</span> the
        following applicant:<br /><span className="font-weight-bolder">{applicant.firstName} {applicant.lastName}.</span> Rejecting an applicant will remove them from the list.</p>
                                    <p className="h4 text-center">Would you like to proceed with rejecting? <strong>This cannot be undone.</strong></p>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>


                                    <button onClick={() => { jobRemoval(applicant, index) }} className="btn btn-outline-secondary" data-dismiss="modal" data-toggle="modal" type="button">
                                        Yes, Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id="applicant-0-notified-modal" tabIndex="-1" role="dialog" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <p className="h1 text-center"><i className="fas fa-check fa-2x text-success"></i></p>
                                    <p className="h4 text-center">{applicant.firstName} {applicant.lastName} has been accepted. Please reach out for the next phase in the application process.</p>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        return (applicantsMapped);
    }


    if (props.location.jobRemoval) {
        console.log('TO BE REMOVED');
    }


    return (
        <section className="custom-header-footer-content-clear-margin d-flex flex-wrap container" id="job-applicant-ui">
            <header className=" w-100 d-flex justify-content-between flex-wrap mb-4">
                <h1 className="h2">{job.title} Applicants</h1>
                <Link to={{
                    pathname: '/jobs',
                    updatedJob: null
                }} className="btn custom-misssion-code-bg font-weight-bold">Back to
                Jobs</Link>
            </header>
            <div className="w-100">
                <table className="table table-borderless table-sm justify-content-center">
                    <tbody>
                        <tr>
                            <th>
                                Location:
                                </th>
                            <td>
                                {job.location}
                            </td>
                        </tr>
                        <tr>
                            <th>Date Posted:</th>
                            <td>{job.date}</td>
                        </tr>
                        <tr>
                            <th>Recruiter:</th>
                            <td>{job.recruiter}</td>
                        </tr>
                        <tr>
                            <th>Salary:</th>
                            <td>${job.minSalary} - ${job.maxSalary}</td>
                        </tr>
                        <tr>
                            <th>Description:</th>
                            <td>{job.description}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {jobApplicants ? displayApplicants(jobApplicants) : <h2 className="m-auto my-5 display-4">No applicants to show</h2>}
        </section>
    )
}




export default ApplicantList;