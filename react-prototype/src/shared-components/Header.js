import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import jobData from '../data/jobData';


const Header = () => {

    return (
        <header>
            <nav className="navbar navbar-expand-lg navbar-light custom-gray-bg fixed-top" id="navbar">
                <Link to="/home"><img src="./assets/mission-code-logo.png" className="navbar-brand custom-logo-width" alt="Mission Code Logo"
                    id="logo" /></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active" id="home">
                            <Link className="nav-link" to="/home">Home</Link>
                        </li>
                        <li className="nav-item active" id="display-jobs">
                            <Link to={{
                                pathname: '/jobs',
                                updatedJob: null
                            }} className="nav-link">Display Job Postings</Link>
                        </li>
                        <li className="nav-item active">
                            <a className="nav-link" href="#">Log Out<span className="sr-only">(current)</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

    )

}

export default Header;