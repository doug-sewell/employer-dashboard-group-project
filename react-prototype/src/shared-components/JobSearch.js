import React from 'react';
const JobSearch = () => {
    return (
        <section className="d-flex justify-content-center align-items-center flex-column w-auto min-vh-100" id="home-content">
            <h1 className="sr-only">Job Search</h1>
            <img src="http://classroom.missioncode.org/projects/employer-dashboard/assets/mission-code-logo.png" alt="Mission Code Logo" />
            <form className="w-100">
                <div className="form-row align-items-center m-0">
                    <div className="w-100 d-flex justify-content-center">
                        <label className="sr-only" htmlFor="search-bar">Job Search</label>
                        <div className="input-group mb-2 d-flex justify-content-center w-75">
                            <input type="text" className="form-control rounded-0 border-right-0 pt-4 pb-4" id="search-bar"
                                aria-label="Search for job postings by keyword, job title, or company"
                                placeholder="Search for job postings by keyword, job title, or company" />
                            <div className="input-group-append">
                                <span className="input-group-text border-left-0 bg-white rounded-0"
                                    id="search-bar_magnifying-glass"><i className="fas fa-search"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    )
}

export default JobSearch;