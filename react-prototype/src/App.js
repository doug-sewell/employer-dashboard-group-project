import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import Jobs from './shared-components/Jobs';
import ApplicantList from './shared-components/ApplicantList';
import Header from './shared-components/Header';
import UpdateJob from './shared-components/UpdateJob';


function App() {
  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route path="/home">
          <Home />
        </Route>
        <Route exact path="/jobs" render={(props) => <Jobs {...props} />} />
        <Route exact path="/applicant-list" render={(props) => <ApplicantList {...props} />} />
        <Route exact path="/update-job" render={(props) => <UpdateJob {...props} />} />
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </BrowserRouter>
  );

}

export default App;
